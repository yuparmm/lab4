# print('\nЗавдання 1: Дано список, що складається з N цілочисельних елементів. Список вводиться з клавіатури. Знайти максимальний елемент. Вивести список на екран у зворотному порядку.')
# numbers = []
# while True:
#     number = int(input('Введіть число (введіть 0 для закінчення): '))
#     if number == 0:
#         break
#     numbers.append(number)
# REVnumbers = reversed(numbers)
# REVlist = list(REVnumbers)
# MAXnum = max(numbers)
# print(f'Введений список: {numbers}')
# print(f'Максимальний елемент: {MAXnum}')
# print(f'Відзеркалений список: {REVlist}')


# print('\nЗавдання 2:Дано список, що складається з N цілочисельних елементів Список вводиться з клавіатури. Переписати всі додатні елементи в другій масив, а решту - в третій. ')
# N = int(input("Введіть кількість елементів: "))
# NUMlist = []
# for i in range(N):
#     element = int(input("Введіть елемент: "))
#     NUMlist.append(element)
# POSlist = []
# NEGlist = []
# for element in NUMlist:
#     if element > 0:
#         POSlist.append(element)
#     else:
#         NEGlist.append(element)
# print(f"Початковий список: {NUMlist}")
# print(f"Список додатніх елементів: {POSlist}")
# print(f"Список від'ємних і нульових елементів: {NEGlist}")



# print('\nЗавдання 3: В списку довжиною 20 обчислити суму елементів з непарними індексами. Вивести на екран список, отриману суму.')
# length = 20
# NUMlist = []
# for i in range(length):
#     element = int(input("Введіть елемент: "))
#     NUMlist.append(element)
# sum = 0
# for i in range(0, 20, 2):
#     sum += NUMlist[i]
# print("Список:", NUMlist)
# print("Сума:", sum)
#



# print('\nЗавдання 4: Сформувати список з 30 випадкових цілих чисел від -100 до + 100. Знайти максимальний елемент списку і його порядковий номер.\n Отримати інший список, що складається тільки з непарних чисел вихідного списку або повідомити, що таких чисел немає.\n Отриманий список вивести в порядку зменшення елементів.')
# import random
# NUMlist = [random.randint(-100, 100) for i in range(30)]
# MAXnum = 0
# MAXindex = 0
# for i in range(30):
#     if NUMlist[i] > MAXnum:
#         MAXnum = NUMlist[i]
#         MAXindex = i
# NEPlist = []
# for i in range(30):
#     if NUMlist[i] % 2 != 0:
#         NEPlist.append(NUMlist[i])
# NEPlist.sort(reverse=True)
# print(f"Вихідний список: {NUMlist}")
# print(f"Максимальний елемент: {MAXnum} з порядковим номером {MAXindex+1}")
# if NEPlist:
#     print(f"Список непарних чисел: {NEPlist}")
# else:
#     print("Непарних чисел немає")



# print('\nЗавдання 5: Сформувати список з 30 випадкових цілих чисел від -100 до + 100. Вивести пари від’ємних чисел, що стоять поруч.')
# import random
# NUMlist = [random.randint(-100, 100) for i in range(30)]
# pairs = []
# for i in range(len(NUMlist) - 1):
#     if NUMlist[i] < 0 and NUMlist[i + 1] < 0:
#         pairs.append((NUMlist[i], NUMlist[i + 1]))
# print(f"Список: {NUMlist}")
# if pairs:
#     print(f"Пари від'ємних чисел: {pairs}")
# else:
#     print("Пар від'ємних чисел не знайдено")


# print('\nЗавдання 6: Дано список з 10 цілих чисел. Знайти максимальний елемент і порівняти з ним інші елементи. Квадрати менших чисел записати в другий список в порядку зменшення.')
# length = 10
# NUMlist = []
# for i in range(length):
#     element = int(input("Введіть елемент: "))
#     NUMlist.append(element)
# MAXnum = NUMlist[0]
# MAXindex = 0
# for i in range(1, len(NUMlist)):
#     if NUMlist[i] > MAXnum:
#         MAXnum = NUMlist[i]
#         MAXindex = i
# KVADlist = []
# for i in range(len(NUMlist)):
#     if NUMlist[i] < MAXnum:
#         KVADlist.append(NUMlist[i] ** 2)
# KVADlist.sort(reverse=True)
# print(f"Список: {NUMlist}")
# print(f"Максимальний елемент: {MAXnum}")
# print(f"Список квадратів менших чисел: {KVADlist}")

# print('\nЗавдання 7: Дано список з 30 випадкових цілих і дробових чисел від -100 до +100. Знайти та вивести мінімальний по модулю елемент. Вивести список на екран в порядку збільшення значення. ')
# import random
# numbers = [random.uniform(-100, 100) for i in range(30)]
# min_abs_value = min(numbers, key=abs)
# min_abs_value_round = round(min_abs_value, 1)
# round_numbers = [round(num, 1) for num in numbers]
# sorted_numbers = sorted(numbers)
# round_sorted_numbers = [round(num, 1) for num in sorted_numbers]
# print(f"Список чисел: {round_numbers}")
# print(f"Мінімальний по модулю елемент: {min_abs_value_round}")
# print(f"Список в порядку збільшення значень: {round_sorted_numbers}")


print('\nЗавдання 8: Дано список з 30 випадкових цілих і дробових чисел від -100 до +100. Сформувати зі списку 10 списків по 3 елементи. Вивести отримані списки в порядку зростання за сумою абсолютних значень окремих елементів.  ')
import random
numbers = [random.uniform(-100, 100) for i in range(30)]
lists = [numbers[i:i+3] for i in range(0, 30, 3)]
lists.sort(key=lambda lst: sum(abs(num) for num in lst))
for lst in lists:
    rounded_lst = [round(num, 2) for num in lst]
    sum_abs = round(sum(abs(num) for num in lst), 2)
    print(rounded_lst, sum_abs)

























